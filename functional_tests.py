from selenium import webdriver
import unittest
from selenium.webdriver.common.keys import Keys


class NewVisitorTest(unittest.TestCase):
    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_create_contact_from_homepage(self):
        """Bob has heard about an online contact app. She
        goes to check out its homepage."""
        self.browser.get('http://127.0.0.1:8000/')
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('HI', header_text)
        """He sees a button that leads to a form
         for creating a contact then clicks it"""
        create_contact_button = self.browser.find_element_by_id('create_contact')
        self.assertEqual(
            create_contact_button.text,
            'Add Contact',
            'Link text is ' + create_contact_button.text
        )
        create_contact_button.click()

        """Bob fills out the form with his details then
        submits it."""
        self.browser.find_element_by_name("name").send_keys('Bob')
        self.browser.find_element_by_name("phone_number").send_keys('09271234567')
        self.browser.find_element_by_name("email_address").send_keys('bob@bob.com')
        self.browser.find_element_by_name("home_address").send_keys('2 6th Street New Manila')
        self.browser.find_element_by_name("favorited").click()
        self.browser.find_element_by_id("submit_add_contact").click()

        import time
        time.sleep(5)

if __name__ == '__main__':
    unittest.main(warnings='ignore')
