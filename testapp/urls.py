from django.urls import include, path
from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'contacts', ContactViewSet)
urlpatterns = [
    # path('', HomePageView.as_view(), name='home_page'),
    # path('add_contact/', ContactCreateView.as_view(), name='create_contact'),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]