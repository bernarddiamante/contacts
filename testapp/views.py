from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from .models import Contact
from .serializers import ContactSerializer
from rest_framework import viewsets


# Create your views here.

class HomePageView(TemplateView):
    template_name = "testapp/home.html"


class ContactCreateView(CreateView):
    model = Contact
    fields = '__all__'


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
