from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string

from .views import HomePageView
from .models import Contact


class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertTrue(found.func, HomePageView)
    # def test_check_for_html_elements(self):


class ContactModelTest(TestCase):
    def test_creating_a_contact(self):
        item = Contact.objects.create(name="Bob",
                                      email_address="bob1@bob.com",
                                      phone_number="09271234567",
                                      home_address="bobville",
                                      favorited=True, )
        pk = item.pk
        # See if it registers into database.
        self.assertEqual(Contact.objects.get(pk=pk).pk,
                         item.pk,
                         'Post did not register into database')
        self.assertEqual(Contact.objects.get(pk=pk).name,
                         item.name,
                         'Names do not match')
