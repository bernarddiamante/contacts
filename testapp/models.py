from django.db import models
from django.urls import reverse

# Create your models here.
class Contact(models.Model):
    name = models.CharField(
        max_length=50,
        blank=True,
        null=True,
    )
    phone_number = models.CharField(
        max_length=15,
        blank=True,
        null=True,
    )
    email_address = models.EmailField(
        blank=True,
        null=True,
    )
    home_address = models.CharField(
        max_length=100,
    )
    favorited = models.BooleanField(
    )

    def get_absolute_url(self):
        return reverse('home_page')

    def __str__(self):
        return self.name
    # kwargs = {'pk': self.pk}